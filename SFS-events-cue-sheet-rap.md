# Clean the room

> Tidy the room and leave out SFS swag for attendees


# Setup Point-of-presence

- Turn on big screen and sound

- Open BBB URL

  * Share webcam

  * Share screen


# Welcome and Purpose

> Welcome to the Software Freedom School.

> Our purpose here is to teach one another how to use and why to choose Free Software.


# What is Free Software?

## Copyright is the legal mechanism afforded to the authors of original works.

> Much like other forms of creative works, copyright also applies to software.

For example,
Under 17 U.S. Code § 106, the exclusive rights granted: 

- (1) the right to reproduce the copyrighted work; 

- (2) the right to prepare derivative works based upon the work; 

- (3) the right to distribute copies of the work to the public; 

- (4) the right to perform the copyrighted work publicly;

- (5) the right to display the copyrighted work publicly.


## Much like copyright law, we have copyleft. 

Copyleft helps us accomplish the idea that: 
Computer users should be free to modify programs to fit their needs, and free to share software, because helping other people is the basis of society. 


A program is free software, for you, a particular user, if:
   - You have the freedom to run the program as you wish, for any purpose.
<!---      * Educational or Nefarious -- both are equally free. -->

   - You have the freedom to modify the program to suit your needs. (To make this freedom effective in practice, you must have access to the source code, since making changes in a program without having the source code is exceedingly difficult.)
<!---      * If you have a piece of software and it isnt the right color for you, you should feel free to purple it up to your heart's content. -->

   - You have the freedom to redistribute copies, either gratis or for a fee.
<!---      * Freedom to Share  -->

   - You have the freedom to distribute modified versions of the program, so that the community can benefit from your improvements.
<!--- * The right to perform or display your work publicly. -->
   



ORIGINAL TEXT FOR RAP
------------------------
> Free Software is software that you are free to:
> - **Use** for whatever purpose you see fit.
> - **Study** how it works, for which access to the source code is a pre-requisite
> - **Share** with your friends.
> - **Modify** to better suit your needs.

* * * 

### - - Free Software means software you can trust. - - 

With so many profit motives at the expense of the user, it's hard to download and run a piece of software and run it with explicit trust... [Who knows who could be on the other end of that software...](https://nextcloud.sofree.us/index.php/s/LKeks92qdwAzkZB) 

* * * 

# Software Freedom School presents events Libre not Gratis

There is a [value for value](https://pastebin.com/raw/i71NPm7m) ask of most events. 

- https://paypal.me/sfs303/

- Or Pay What You Choose: pwyc@sofree.us


Almost every SFS class is PWYC (Pay What You Choose). You know what your budget is, what your values are, and your talents and time that may be worth more than your money. Whether you pay what was asked, or more, or less, or differently, is entirely up to you, your budget, your values and priorities. I trusted you before I met you.



### SFS class materials are free-libre like Free Software.

> SFS class deliveries are *not* free-gratis like the pretzels in a bar. 

> The ask for today's class is: ___ USD/BTC/whatever

> You may pay the ask or Pay What You Choose: more, less, or differently.


#### Common ways to Pay What You Choose are:

> - Pay with **crypto-currency**
> - Send a **postcard** with a picture of you saving a whale or a kitten.
> - **Teach** a class. This is our favorite, and you can make money at it!

* * * 

# Restrooms, Refreshments, Recycling

### The Swag is free. 

If you would like a pen, paper, button, or sticker -- they're all free! Take and enjoy!!



### Upcoming Events / We Take Requests

> SFS has delivered several custom classes on request. If you or your employer would like to commission a custom class, in-person, on-site, or remote, just let us know. If it's Free Software, [we teach it](https://gitlab.com/sofreeus/sfs-bizops/-/blob/master/sfs-class-catalog.md).

* * * 

# Greet and then introduce everyone
- Name
- Role and/or Goal(s)



