# Cloud-init is a a great way to run in the public cloud. 

## It allows you to create your very own Linux deployment.

### Automatically handle a few things everytime you deploy your own Linux image. 


* * * 

Let's use debian for our example.

* * *


### First step, is it already installed with your distro:

`dpkg --get-selections | grep cloud-init`



#### Once installed:

`cd /etc/cloud`

`ls -l`



#### Copy the default config:

`cp cloud.cfg cloud.default.cfg.bak`



https://cloudinit.readthedocs.io/en/latest/reference/modules.html



### Ok, let's clean this default file up, scroll to the bottom of the document:

Let's take a look at these modules. There are plenty of these that we dont need...

`- byobu`



Scrolling down...

`- puppet`

`- chef`

`- mcollective`

`- salt`



### Great! Scrolling back up...

Under the 'users' section...



Default users vary between distros, debian it's debian, ubuntu it's ubuntu. This is a security nightmare. Let's disable this section.

Comment out `   - default` and be sure to leave the `users:` before it.



#### To make sure that default user is never created, we need to scroll down far in the config...

find the:


```
distro: xzyxvrz
default_user:
	name: xzyxvrz
```


##### and comment all of that out. Everything after distro, about 'default_user' until you get to the network section.



## We can save this file and continue our work, BUT DO NOT REBOOT YET.


### Let's generate a password to use with our cloud-init config.

`which mkpasswd`

if mkpasswd is not installed, you can install it with `apt install whois`



Create the password for the new user in our cloud-init config...

`mkpassword -m sha-512`


* * * 

So now we have our password hash, and those of you who've memorized the sha-512 rainbow table of all dictionary words.... yes, you're right... the password is: password


#### Go back to editing the `/etc/cloud/cloud.cfg`



Let's add a new user and define all of the different elements of that user.



### Editing the `users:` section, 


```
users:
#   - default
   - name: pcuser
     lock_passwd: False
     passwd: $6$0xX4putyourpasswordyouhashedabovehere4Xx0$6$
	 gecos: PC-User
	 ssh_authorized_keys:
	   - ssh-ed25519 AAAAC3Nzlfytfdc6590alu85siop788ucgt
     groups: [adm, audio, video, cdrom, sudo, dip, plugdev, users]
     sudo: ["ALL=(ALL) NOPASSWD:ALL"]
	 shell: /bin/bash
```

- lock_passwd disables password login for the user, it defaults to true. 

- run `id 1000` to see what groups a regular joe user should be a part of... for filling out the groups section.



### Moving back down to the modules section, you can add your timezone right now.


`- timezone "America/Denver"`


Find the area containing `preserve_hostname: false` and enter:

```
hostname: myhostname.mydomain.com
manage_etc_hosts: true
```


### At the very end of the file,

```
bootcmd:
   - echo date > /etc/firstbookdate; echo " $(date -R) " | 'figlet' -c -k -w 180 -f Doh > /etc/firstbootdatecool

packages: 
   - git
   - tmux
   - figlet
```



## Cloud-initc doesn't start when no data source is present. It needs a data source, it bypass that, we're going to do this:

`nano ./cloud.cfg.d/99-fake_cloud.cfg`


```
# configure cloud-init for NoCloud
datasource_list: [ NoCloud, None ]
datasource:
  NoCloud:
    fs_label: system-boot
```


### Now, we've been very busy. Let's clean up our install of cloud-init and reset it back to start:

`cloud-init clean`


#### To ensure cloud-init can do it's thing with networking, we have to remove the systemd network file that it is currently using:

`rm /etc/systemd/network/99-default.link`

##### Now that symbolic link is gone and cloud-init and set the networking. 

`cloud-init clean`


Now we're ready to break something. So normally what we'd do is shut the VM down, take a snapshot of it (before its firstbootdate).

##### BUT we dont need to write this in stone, let's run the config and make sure it was correct...

`cloud-init init`

### Drum roll, moment of truth....

let's see what happens...

WOAH THAT WAS FAST!


* * * 

- We can check the hostname file, the /etc/hosts and /etc/firstbootdate


SUCCESS! We can demo the software!

* * * 



## But what about doing this inside of a hypervisor, you know, for like larger scale deployments...

### Ok, I guess we can do a Proxmox Demo...


First, pick the cloud template you want to use for your new proxmox template. 

Next, make the VM

`qm create 8000 --memory 2048 --name cloud-template --net0 virtio,bridge=vmbr0`

NOTE: vmbr0 puts this on David's network

Super, we have a VM, but now we need to give it the approprate disk. Download your cloud template and import that to your VM

`wget https://cloud-images.ubuntu.com/lunar/current/lunar-server-cloudimg-amd64.img`

`qm importdisk 8000 lunar-server-cloudimg-amd64.img local-lvm`

The last entry on that line tells the server where you want to store that disk. Make sure it matches.

You should see a progress indicator.

After that's complete, add the scsi controller, and a scsi drive...

`qm set 8000 --scsihw virtio-scsi-pci --scsi0 local-lvm:vm-8000-disk-1`

You should now see a SCSI controller and a drive.


But that's not the drive that's going to store our configs. We need to create that drive now:

`qm set 8000 --ide2 local-lvm:cloudinit`



Now we have to set the appropreate boot device:

`qm set 8000 --boot c --botdisk scsi0`



And most cloud vendors have this turned on by default, but if we want to see the output in the VNC section of Proxmox, we need to add this:

`qm set 8000 --serial0 socket --vga serial0`


Now we can initalize virtualmachines with the values in the Cloud-init section of Proxmox.

If you dont set anything, you wont have any network access at all. Be sure you check all the values.

Dont start this machine up yet.

If you start it up you'll be bootstrapping the VM with a UUID and a MachineID. And if that happens, you'll have the same machine config across all your templates. 


This is a one-way ticket when you convert to template.

Once you're done you'll see it has a slightly different icon.




