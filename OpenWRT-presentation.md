% Alternative Router Firmware
%      Marcus Holtz     
%   2023 April 1  

# Alternative Router Firmware
## Why would anyone do this?
Let's [google the alternative](https://www.google.com/search?client=firefox-b-1-d&q=cvedetails+household+routers){target="_blank"}, shall we? 
🔎   🔦  🕵  😧


#### Support lacks for home consumer routers. 
The vendor expects you to upgrade to a shiny new one ASAP.... or atleast by [the year 2038.](https://openwrt.org/releases/22.03/notes-22.03.0-rc1#year_2038_problem_handled){target="_blank"}


By contrast: 
OpenWRT supports **more than 1,500 devices**. 


All kept up to date. 


## Let's talk about the different router firmware that's out there
Specifically, in this talk, we're refering to **embedded firmware**. 


This means, modifing the firmware that is preinstalled by the manufacturer in the router’s read-only memory (ROM).


There's a lot of different router firmware floating around the internet: 


- [Gargoyle](https://en.wikipedia.org/wiki/Gargoyle_(router_firmware)) 

- [Asuswrt-Merlin](https://www.asuswrt-merlin.net/)

- [DD-WRT](https://en.wikipedia.org/wiki/DD-WRT)

- [Tomato](https://en.wikipedia.org/wiki/Tomato_(firmware))

- [OpenWrt](https://en.wikipedia.org/wiki/OpenWrt)


We're going to talk about the last three.



## Broadcom chipset based routers
### DD-WRT
DD-WRT is Linux-based firmware for wireless routers and access points. Originally designed for the Linksys WRT54G series, it now runs on a wide variety of models.
DD literally means nothing. The original German development team just used it because they saw it on car licesense plates everywhere. And, the WRT from the most popular router at the time the Linksys WRT54G series.

#### DD-WRT Licensure 
DD-WRT has a separate professional license, meant for commercial usage. DD-WRT has a license agreement and NDA in place with Broadcom that allow usage of better, proprietary, closed source wireless drivers (binary blobs) which they are not allowed to redistribute freely. Otherwise it's mostly known for being licensed under the GNU General Public License version 2.


## DD-WRT Features 
- [Guest WiFi](https://wiki.dd-wrt.com/wiki/index.php/Guest_WiFi_%2B_abuse_control_for_beginners)
- Web Management Interface
- Bandwith Monitoring with YAMon
- OpenVPN / Wireguard


### DD-WRT Demo!

#  DD-WRT Interface screenshot
--- 
![ ](https://i.ibb.co/5KyVPx9/ddwrt.png){width=100% height=100%}



## Tomato Firmware
Tomato is much simplier and offers some different features.... but.... basically, this exists because someone along the lines got a copy of some Broadcom code (like DD-WRT) and have been sneakishly using it since. Broadcom has not released any FOSS drivers. Broadcom doesn’t support open-source much at all.


#### FreshTomato / AdvancedTomato Features
- Guest WiFi
- Web Management Interface 
- Bandwith limiting / stats
- Webserver (nginx)
- OpenVPN

### FreshTomato Demo!

#  FreshTomato Interface screenshot
--- 
![ ](https://i.ibb.co/FWfvr5Q/tomato-router.png){width=100% height=100%}



## OpenWRT supported routers
OpenWrt use only FOSS drivers.
OpenWrt is a highly extensible GNU/Linux distribution.OpenWrt is built from the ground up to be a full-featured, easily modifiable operating system for your router. In practice, this means that you can have all the features you need with none of the bloat, powered by a modern Linux kernel.
Instead of trying to create a single, static firmware, OpenWrt provides a fully writable filesystem with optional package management. 

### Supported vendors' system on a chip
- Qualcomm Atheros
- MediaTek / Ralink
- Marvell
- Realtek
- Broadcom (FOSS support for the Broadcom does not exist.)



## Table of hardware
This is the main Table of Hardware, a showcase listing some of the devices that are supported by OpenWrt. 
[https://openwrt.org/toh/start](https://openwrt.org/toh/start){target="_blank"}


### Install OpenWRT Demo!
								( ͡❛ ͜ʖ ͡❛) 👉        📡



### OpenWRT First Install Commands
```

reboot
opkg update
opkg install luci-ssl
opkg install luci-theme-material

 
```



## OpenWRT - Features!
- [Ad Blocking](https://openwrt.org/docs/guide-user/services/ad-blocking){target="_blank"}

Network-wide ad blocking: content filtering to reduce ads, reduce bandwidth usage, reduce tracking and increase privacy. 


- [Bandwith Monitoring](https://openwrt.org/packages/pkgdata/luci-app-nlbwmon)

 
`opkg install luci-app-nlbwmon` and you get instant bandwidth stats in the LuCI web interface.


- [WiFi Schedule](https://openwrt.org/packages/pkgdata/luci-app-wifischedule)

Turn your WiFi on and off according to a schedule.


- [DNS over HTTPs](https://openwrt.org/packages/pkgdata/luci-app-https-dns-proxy)

Light-weight DNS-over-HTTPS, non-caching translation proxy.


- [Guides for OpenWRT](https://openwrt.org/docs/start)

Why stop here, there are guides for just about everything!


- [List of all the packages for OpenWRT](https://openwrt.org/packages/index/start) also [Repology](https://repology.org/projects/?inrepo=openwrt_22_03_x86_64)

Take a look at all of the packages available. 




## Upgrade OpenWRT
Updates via flash. 
Why flash? 
These are cheap devices. Therefor they have very little memory.
To fit Linux on these, squashfs is used. That is a read-only file system.
So updates are done via iso img and dumped into memory. 


### DevOps Upgrade, find your device, build an image!
[firmware-selector.openwrt.org](https://firmware-selector.openwrt.org){target="_blank"} is a full build system allowing you to customize the image you need right from the web interface. 


## Automate everything
You can forgo the downloading and flashing of the rom if you like. There are utilities that help with unattended upgrades:
"auc" and "luci-app-attendedsysupgrade" 

- [auc](https://openwrt.org/packages/pkgdata/auc){target="_blank"} will do upgrades from the terminal

- [luci-app-attendedsysupgrade](https://openwrt.org/packages/pkgdata/luci-app-attendedsysupgrade){target="_blank"} can do updates just from the web interface

Using the (optionally self-hosted) [attendedsysupgrade Server](https://github.com/openwrt/asu){target="_blank"}, these utilities above will go out to the build system, build a custom image with all your packages for you, and deliver it for update. 



## How does the filesystem work on these small routers?
I'm glad you asked.
An overlay filesystem is created to save your config.
`mount to view your /rom and /overlay`
Again, everything is saved in /overlay, if you `touch /newfile_here_in_root` it will appear under /overlay


`/rom/usr/lib/opkg/lists` 
will display everything that was installed when compiled


`/overlay/upper/usr/lib/opkg/lists` 
will display everything that was installed after the rom update



### Where is my configuration stored?
OpenWrt's central configuration is split into several files located in `/etc/config/`
[Click for the UCI system](https://openwrt.org/docs/guide-user/base-system/uci){target="_blank"} used to centralize configuration of OpenWRT.



## OpenWRT Extended Features
- [Airmon-ng](https://openwrt.org/packages/pkgdata/airmon-ng){target="_blank"} / [Aircrack-ng](https://openwrt.org/packages/pkgdata/aircrack-ng){target="_blank"}

WLAN tools for breaking 802.11 WEP/WPA keys


- [Wavemon](https://openwrt.org/packages/pkgdata/wavemon){target="_blank"}

Visualize and monitor your wireless network.


- [Git](https://openwrt.org/packages/pkgdata/git){target="_blank"}

Check your config into a repository.


- [Cloudbased IPS](https://openwrt.org/docs/guide-user/services/crowdsec){target="_blank"}

Crowdsec allows you to detect peers with malevolent behaviors and block them with the help of cloud based pattern recognition. 


- [Webserver](https://openwrt.org/docs/guide-user/services/webserver/uhttpd){target="_blank"}

 
`opkg install luci-app-uhttpd uhttpd uhttpd-mod-ubus luci-ssl-openssl ddns-scripts-gandi` and now you have a webserver.




Hotel Wifi Demo!!!
--- 
Seperation between guests
`(Client Isolation Mode)`
Clients communicate only with the AP and not with other wireless clients. 


### Guest WiFi Network
```
OpenWRT > Network > Interfaces > Add New Interface
Name: "Guest"
Protocol: Static IP
Assign the static ip in the new window, then ...
click on the tab labeled, Firewall Settings
Use the dropdown to type a new firewall zone: "Guest", then ...
click the DHCP Server tab
Click "Setup DHCP Server", and Save
Save & Apply
Network > Wireless
ESSID: "YourSocialSecurityNumberHere"
Network: "Guest"
Click on the 'Advanced tab' and check "Isolate Clients"
```
`OpenWRT > Network > Wireless > Advanced Settings > Isolate Clients`



## Secure WiFi Network
### Block Guest Access to Router's Management
```
Network > Firewall
Edit the Guest firewall zone
Input: Reject
Output: Accept
Forward: Reject
Allow forward to destination zone: WAN
Click on the 'Traffic Rules' tab
```
```
At the bottom of the page, click 'Add'
Name: "Guest-DHCP"
Protocol: UDP
Source: "Guest"
Destination zone: "Device (input)"
Destination port: 67
Click 'Save'
```
```
At the bottom of the page, click 'Add'
Name: "Guest-DNS"
Source: "Guest"
Destination zone: "Device (input)"
Destination port: 53
Save & Apply
```

If you have multiple guest SSIDs, say for 5G, you can bridge the "Guest" interface under:
`OpenWRT > Network > Interfaces > "Guest" > Physical Settings tab > Bridge interfaces (and check the boxes)`



## Public Guest WiFi Access
### Captive Portal
```
System > Software
Filter for available package: "OpenNDS"
Install

/etc/config/opennds
enable editing of the depricated splash page by uncommenting the line: 
"option allow_legacy_splash '1' and changing it to 1"

change the interface to the correct "Guest" WiFi network, uncomment and edit: 
"option gatewayinterface 'wlan1'"

edit the GatewayName to reflect the host, uncomment and edit: 
"option gatewayname 'OpenWRT openNDS'"

$ service opendns restart
```



## 




#### OpenNDS More Info:
[https://openwrt.org/docs/guide-user/services/captive-portal/opennds](https://openwrt.org/docs/guide-user/services/captive-portal/opennds){target="_blank"}

[https://opennds.readthedocs.io/en/stable](https://opennds.readthedocs.io/en/stable){target="_blank"}

[Van_Tech_Corner--YouTube---OpenWRT   Captive_Portal_-_WiFi_Splash_Page](https://www.youtube.com/watch?v=b7vTY1k8iGo){target="_blank"}




## SSID suggestions
Emoji Based SSIDs: [Verified under 32bytes](https://onlineunicodetools.com/count-unicode-characters){target="_blank"}
```
ShowerCamera🚿
idk...whatever....lol.....🤷
ₕₒw dᵢd ᵢ gₑₜ ₕere
🔧 𝐖𝐫𝐞𝐧𝐜𝐡
🏞 Forest 🏕
𝓟𝓪𝓷𝓽𝓼👖
_‗🏝️‗⛱ ☀   ☁
░100‱NotAVirus.exe⁈‼░
Kids Wifi    ʕ•ᴥ•ʔ
```



Some of the best SSIDs suggested by folkes on reddit.com:
```
Large Packet Collider
The Promised LAN 2.4GHz
Routers of Rohan
Abraham_Linksys
John_Wilkes_Bluetooth
GetOffMyLan
LANakin_Skywalker
Loading...
Connecting...
```



## This is great! 

### How can I support OpenWRT?


#### I dont know... uhhh... 


[They sell t-shirts from Spain](https://www.freewear.org/OpenWrt){target="_blank"}



## THANKS for joining me!


![](https://media.giphy.com/media/3oKIPsx2VAYAgEHC12/giphy.gif){.center}


