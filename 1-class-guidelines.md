# SFS Class Guidelines

## Setup a Classroom

* [ ] Tables and chairs
* [ ] Power: extension cords, power strips
* [ ] Notepads(hex) and pencils
* [ ] Stickers
* [ ] Karma Envelopes
* [ ] ? Network stuph: switch, cables, wireless
* [ ] Projector / Big Screen
* [ ] ? Whiteboard
* [ ] ? Duct tape, zip ties, tools

## SFS Student Prepareedness Guidelines

- Divide the labs. Be clear about ownership over the teaching chunks.

- Limit interruptions and cross-talk.  When it's the other person's turn, don't offer corrections or commentary unless requested.

- Discuss in advance the approach you will use to manage a jr/sr pairing.
  * When will the jr teacher seek additional commentary from the sr teacher?
  * How will you mitigate an over-reliance on the sr teacher?

- Discuss a plan for MBWA (management by walking around) during the labs.

- Discuss a plan for presentations


## Aaron's Steps

0. https://gitlab.com/sofreeus/sfs-bizops/-/blob/master/values.md

1. https://gitlab.com/sofreeus/class-template/-/blob/master/Usage.md

2. https://gitlab.com/sofreeus/class-template/-/blob/master/README.md

3. https://gitlab.com/sofreeus/sfs-bizops/-/blob/master/event-delivery-guide.md

4. https://gitlab.com/sofreeus/sfs-bizops/-/blob/master/event-template.md

5. https://gitlab.com/sofreeus/sfs-bizops/-/blob/master/sfs-points-of-presence.md
